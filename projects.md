---
title: Projects
---

# Projects

## Work
I've founded a few companies:

<ul>
<li><a href="https://www.inner-product.com/">Inner Product</a> is a consultancy focused on Scala, primarily serving the US.</li>
<li><a href="https://underscore.io/">Underscore</a> is a consultancy focused on Scala, primarily serving the UK.</li>
<li><a href="http://www.mynaweb.com/">Myna</a> was an A/B testing SaaS. Our core innovation was using bandit algorithms to make more efficient use of data.</li>
<li><a href="https://untyped.com/">Untyped</a> is a consultancy founded by my friend Dave and I. Untyped doesn't currently do much work directly, but it's involved in other projects.</li>
</ul>

## Books

<dl>
<dt><a href="https://creativescala.org/">Creative Scala</a></dt>
<dd>An introduction to functional programming and generative art, aimed at people new to programming. Creative Scala is heavily inspired by <a href="https://htdp.org/">HtDP</a>, and like HtDP strives to present programming as a systematic process.</dd>
<dt><a href="https://underscore.io/books/essential-scala/">Essential Scala</a></dt>
<dd>An introduction to Scala for experienced developers, Essential Scala revolves around four core techniques in effective Scala programming: algebraic data types, structural recursion, sequencing computations, and type classes.</dd>
<dt><a href="https://underscore.io/books/scala-with-cats/">Scala with Cats</a></dt>
<dd>This book covers core functional programming abstractions such as monoids and monads, and shows how they can be used in the small and in the large.</dd>
</dl>

## Code

At the moment my main open source project is <a href="https://github.com/creative-scala/doodle">Doodle</a>, a Scala library for visualisation and creative coding. Other projects can be found on my <a href="https://github.com/noelwelsh/">Github account</a>.

## Talks

<dl>

<dt>[Tips for Teaching Scala](/posts/2019-06-20-tips-for-teaching-scala.html), 2019</dt>
<dd>[Scala Days Laussane][scala-days-lausanne-2019]</dd>

<dt>Differentiable Functional Programming, 2018</dt>
<dd>Scala Days NYC, Scala Days Berlin, Scala eXchange</dd>

<dt>Maths for Programming, Fun, and Creativity, 2017</dt>
<dd>With Dave Gurnell, [Lambda World Cadiz][lambda-world-cadiz-2017]</dd>

<dt>Uniting Church and State: FP and OO Together, 2017<dt>
<dd>[Scala Days Copenhagen][scala-days-copenhagen-2017], Scala Days Chicago, Scala eXchange</dd>

<dt>The Structure of Programming Language Revolutions, 2016</dt>
<dd>[Scala Days Berlin 2016][scala-days-berlin-2016], [Scala Days NYC 2016][scala-days-nyc-2016]</dd>

<dt></dt><dd><em>To be continued...</em></dd>
</dl>

[lambda-world-cadiz-2017]: https://www.youtube.com/watch?v=4AbSJfu6S8M
[scala-days-copenhagen-2017]: https://www.youtube.com/watch?v=IO5MD62dQbI
[scala-days-berlin-2016]: https://www.youtube.com/watch?v=bL-CcjKW1lw
[scala-days-nyc-2016]: https://www.youtube.com/watch?v=AL1J5AT4pfY
[scala-days-lausanne-2019]: https://portal.klewel.com/watch/webcast/scala-days-2019/talk/6/
