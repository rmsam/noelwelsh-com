---
title: How to Write an Abstract
---

How do you write an abstract for a (industry) conference talk proposal? I like to use a three paragraph structure. The first paragraph gives a quick overview of what attendees can expect to learn if they attend the talk. The second paragraph gives motivation and background---why should attendees care about what I have to say? The third and final paragraph goes into more detail on the points I intend to cover.

<!--more-->

When writing an abstract I keep two audiences in mind: the conference attendees and the conference organisers. Luckily they have the same goal (working out if I have something interesting say before seeing my talk) but the typical conference attendee won't get past the first paragraph, whereas I hope the organisational committee will read in a bit more depth. This is why I want the first paragraph to overview and sell my talk---so attendees who only read that far can decide if they want to attend. It's also really helpful to have thought about the main points of the talk when writing the abstract. The organisers want a bit more detail, so the job in the remaining paragraphs is to convince them that the problem I'm addressing is important (and hence will get an audience) and that I have something coherent to say about it.

I don't always use the three paragraph structure. Sometimes the conference limits the length of abstracts so I have to chop it down, and something the three paragraph structure just doesn't fit. But I find it a good way to approach writing an abstract even if I don't strictly follow the structure.

